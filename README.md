# SAE-dev_tests



## Pour commencer

Afin de pouvoir lancer les batteries de tests sur vos fichiers vous aurez à effectuer les action suivantes:
- [ ] Créer un fork de ce dépôt en cliquant sur le bouton **fork** et en remplissant le formulaire associée.
- [ ] Cloner votre projet créé par le fork sur votre machine

```
cd mon_repertoire
git clone https://gitlab.com/mon_projet_fork
```
- [ ] Copier vos fichiers case.py joueur.py equipement.py protection.py trojan.py dans le répertoire où se trouve le projet forké
```
cd repertoire_de_mon_projet
cp case.py joueur.py equipement.py protection.py trojan.py mon_repertoire/mon_projet_fork
```
- [ ] Ajouter les fichiers au dépôt et les pousser
```
cd mon_repertoire/mon_projet_fork
git add *.py
git commit -a -m "ajout des fichiers à tester"
git push
```
- [ ] Vérifier le résultat du push sur le GitLab de votre fork en cliquant sur le menu CICD -> Pipelinespuis en cliquant sur le statut du job le plus récent (le 1er de la liste). Vous pouvez voir le détail des résultats en cliquant sur la colonne Tests. 

### Par la suite

Lorsque vous avez avancé dans votre projet et que vous souhaitez relancer les tests, il vous suffit de recopier vos fichiers corrigés vers le dépôt de test, valider les changements et les pousser.
```
cd repertoire_de_mon_projet
cp case.py joueur.py equipement.py protection.py trojan.py mon_repertoire/mon_projet_fork
cd mon_repertoire/mon_projet_fork
git commit -a -m "un beau message"
git push
```
